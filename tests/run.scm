(use test plan plan-parallel srfi-1 srfi-18)

(define foo-count 0)
(define bar-count 0)
(define baz-count 0)
(define qux-count 0)

(define a-plan
  (plan
   (lax 1)
   (foo (lax)
        (set! foo-count (+ foo-count 1))
        lax)
   (bar ()
        (set! bar-count (+ bar-count 1))
        2)
   (baz (foo qux zig)
        (set! baz-count (+ baz-count 1))
        (list (+ 1 (-  zig qux foo))))
   (qux (bar)
        (set! qux-count (+ qux-count 1))
        (* bar bar))
   (zig (foo bar qux)
        (* foo (+ qux 2)))))

(test-group "realizing a plan"
  (test 1 (realize-plan a-plan 'foo))
  (test '(2) (realize-plan a-plan 'baz))
  (test '(2) (realize-plan a-plan 'baz))
  (test 2 baz-count))

(test-group "realizing multiple steps"
  (set! foo-count 0)
  (set! bar-count 0)
  (set! qux-count 0)
  (test '(1 2 4 4) (realize-plan a-plan '(foo bar qux qux)))
  (test 1 foo-count)
  (test 1 bar-count)
  (test 1 qux-count))

(test-group "realizing all steps in one go"
  (let ((p (realize-plan a-plan)))
    (test 1 (alist-ref 'foo p))
    (test 2 (alist-ref 'bar p))
    (test '(2) (alist-ref 'baz p))
    (test 4 (alist-ref 'qux p))
    (test 6 (alist-ref 'zig p))))

(test-group "passing arguments"
  (define another-plan
    (plan (x 2)
          (y (x) (* x x))
          (z (x y) (+ x y))))
  (test-assert
   (lset= equal?
          '((z . 3) (y . 2) (x . 1))
          (realize-plan another-plan x: 1 y: 2)))
  (test 6 (realize-plan another-plan 'z))
  (test 2 (realize-plan another-plan 'z x: 1 y: 1))
  (test 'nice (realize-plan another-plan 'z z: 'nice)))

(test-group "re-usig a plan realization result"
  (set! baz-count 0)
  (let ((result (realize-plan a-plan 'baz #t '((lax . 2)))))
    (test '(7) (alist-ref 'baz result))
    (test '(7) (realize-plan a-plan 'baz result))
    (test-assert (list? (realize-plan a-plan result)))
    (test 1 baz-count)))

(test-group "plan-let"
  (plan-let
   ((foo (bar qux) (* bar qux))
    (bar (baz) (+ baz baz))
    (baz 5)
    (qux 99))
   (test foo 990)
   (test bar 10)
   (test baz 5)
   (test qux 99)))

(test-group "parallel realization"
  (define p
    (plan
     (con 1)
     (foo (con) (thread-sleep! 0.5) con)
     (bar () (thread-sleep! 0.5) 2)
     (baz (foo bar) (+ foo bar))
     (qux (bar baz) (* 2 bar baz))))

  (set! start (current-milliseconds))
  (let ((qux (realize-plan-parallel p 'qux)))
    (test 12 qux)
    (test-assert (< (- (current-milliseconds) start) 1000))))

(test-group "missing step"
  (define mplan
    (plan (x (y) 1)))
  (test 'y
        (condition-case (realize-plan mplan)
          (exn (plan exn missing-step)
               (get-condition-property exn 'missing-step 'step)))))

(test-group "step realization errors"
  (define eplan
    (plan (a () 1)
          (b (a) (+ a 2))
          (c (b) (/ b 0))
          (d (a c) (+ a c))))
  (for-each
   (lambda (realize)
     (receive (arithmetic? failed-step results)
         (condition-case ((cdr realize) eplan 'd)
           (exn (plan exn step-failed arithmetic)
                (values
                 ((condition-predicate 'arithmetic) (get-condition-property exn 'step-failed 'cause))
                 (get-condition-property exn 'step-failed 'step)
                 (get-condition-property exn 'step-failed 'results))))
       (test-assert arithmetic?)
       (test failed-step 'c)
       (test-assert (lset= equal? '((b . 3) (a . 1)) results))))
   (list (cons "realize-plan" realize-plan)
         (cons "realize-plan-parallel" realize-plan-parallel))))

(test-group "direct interface"
  (define the-plan
    (make-plan
     `(("some" #f . map)
       ("steps" ("some") . ,(lambda (_ s)
                              (cons (car s) 1)))
       (result ("some" "steps") . ,(lambda (_ s)
                                     (cons 'done s))))))
  (test (list 'done 'map '(map . 1))
        (realize-plan the-plan 'result)))

(test-exit)
