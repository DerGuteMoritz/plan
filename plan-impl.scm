@(egg "plan")
@(description "A data structure for representing process plans.")
@(author "Moritz Heidkamp")
@(email "moritz@twoticketsplease.de")
@(username "syn")

@(heading "Overview")

@(text "''A plan is typically any diagram or list of steps with timing
and resources, used to achieve an objective.'' ― [[http://en.wikipedia.org/wiki/Plan]]

This extension provides a {{plan}} data structure which is inspired by
[[http://blog.getprismatic.com/blog/2012/10/1/prismatics-graph-at-strange-loop.html|Prismatic's
\"Graph\"]]. It represents processes and their
interdependencies. Plans can be inspected, augmented, or realized. The
source code can be found on
[[https://bitbucket.org/DerGuteMoritz/plan|Bitbucket]].")

@(heading "API")

(import chicken scheme)
(use srfi-1 data-structures extras matchable persistent-hash-map)

(define (plan-steps plan)
  @("Returns a {{plan}}'s list of steps as passed to [[#make-plan|{{make-plan}}]]."
    (@to "(<step> ...)")
    (@no-source))
  (void))

(define (plan? datum)
  @("Checks whether {{datum}} is a plan."
    (@to "boolean")
    (@no-source))
  (void))

(define-record-type plan
  (%make-plan steps)
  plan?
  (steps plan-steps))

(define (make-plan steps)
  @("Constructs a plan record with the given list of {{steps}} of the following form:

  steps          = (<step> ...)
  step           = <constant-step> | <dependent-step>
  constant-step  = (<name> #f . <value>)
  dependent-step = (<name> (<dependency> ...) . proc)
  dependency     = <name>
  value          = datum
  name           = datum

Semantically, a step is a description of how to realize a (sub-)result
of a plan. It consist of the following components:

* A {{name}}, which may be any kind of datum comparable with
  {{equal?}}, usually a symbol or a string. However, pairs as step
  names should be avoided.

* Zero or more dependencies which are specified as a list of step
  names. Upon realization (see [[#realize-plan|{{realize-plan}}]]),
  dependencies will be realized before a dependent step is
  realized. Alternatively, {{#f}} can be used to signify that this step
  has a constant result without any dependencies.

* A realization result which can either be a constant {{value}} or
  procedure {{proc}} which produces a result from the given
  dependencies. {{proc}} must accept two arguments, the first being the
  step's {{name}} and the second being the list of realized dependency
  results in the order they are specified."
    (@to "plan")
    (@no-source))
  (%make-plan
   (if (map? steps)
       steps
       (alist->map steps))))



(define-record-printer (plan p out)
  (display "#<plan " out)
  (for-each (match-lambda
             ((? string? x)
              (newline out)
              (display x out))
             ((name deps . value)
              (write (cons name
                           (if deps
                               (list deps '...)
                               (list value)))
                     out)))
            (intersperse (map->alist (plan-steps p)) "       "))
  (display ">" out))

(define (kvlist->steps kvlist)
  (let loop ((kvlist kvlist))
    (if (null? kvlist)
        '()
        (cons (cons (string->symbol (keyword->string (car kvlist)))
                    (cons #f (cadr kvlist)))
              (loop (cddr kvlist))))))

(define (alist->steps alist)
  (map (match-lambda ((k . v) (cons* k #f v))) alist))

(define-syntax plan*
  (syntax-rules ()
    ((_ (step value) more-steps ...)
     (map-add! (plan* more-steps ...) 'step (cons #f value)))
    ((_ (step (dependency ...) body ...) more-steps ...)
     (map-add! (plan* more-steps ...)
               'step
               (cons '(dependency ...)
                     (match-lambda*
                      ((_ (dependency ...))
                       body ...)))))
    ((_) (map->transient-map (persistent-map)))))


(define-syntax plan
  @("Convenience syntax for constructing plans. Its
body has the following structure:

  step           = <constant-step> | <dependent-step>
  constant-step  = (<name> value)
  dependent-step = (<name> (<dependency> ...) expression ...)
  dependency     = <name>
  name           = symbol

See [[#make-plan|{{make-plan}}]] for explanation of steps and
dependencies. Note that with this form, step names must be
symbols. For dependent steps, those names are then lexically bound
when {{expression ...}} is evaluated upon realization."
    (step... "")
    (@to "plan")
    (@no-source))
  (syntax-rules ()
    ((_ step ...)
     (make-plan (persist-map! (plan* step ...))))))

(define (step-ref steps name)
  (or (map-ref steps name)
      (abort
       (make-composite-condition
        (make-property-condition
         'exn
         'message "Missing plan step"
         'arguments (list name (map-keys steps)))
        (make-property-condition 'plan)
        (make-property-condition 'missing-step 'step name)))))

(define (step-dependencies to-realize steps)
  (let loop ((to-realize to-realize)
             (deps '()))
    (if (null? to-realize)
        deps
        (let ((name (car to-realize)))
          (if (find (lambda (dep) (equal? name (car dep))) deps)
              (loop (cdr to-realize) deps)
              (let ((step-deps (or (car (step-ref steps name)) '())))
                (loop (append step-deps (cdr to-realize))
                      (cons (cons name step-deps) deps))))))))

(define (make-realization-promise step-name step-result result-ref results dependencies body)
  (delay
    (handle-exceptions exn
      (abort (make-composite-condition
              exn
              (make-property-condition 'plan)
              (make-property-condition
               'step-failed
               'step step-name
               'results (map-collect
                         (lambda (name step)
                           (cons name (step-result step)))
                         results)
               'cause exn)))
      (body step-name
            (map (lambda (x)
                   (result-ref x results))
                 dependencies)))))

(define (realize-steps to-realize realize-step step-result result-ref)
  (let loop ((to-realize to-realize)
             (results (persistent-map)))
    (if (null? to-realize)
        results
        (match-let (((name dependencies . body) (car to-realize)))
          (let ((value (if dependencies
                           (realize-step
                            (make-realization-promise
                             name
                             step-result
                             result-ref
                             results
                             dependencies
                             body))
                           body)))
            (loop (cdr to-realize)
                  (map-add results name (cons dependencies value))))))))

(define (to-realize-arg? to-realize)
  (or (atom? to-realize)
      (and (pair? to-realize) (atom? (car to-realize)))))

(define (destructure-realization-args args)
  (receive (rest kwargs) (break keyword? args)
    (let* ((inputs (kvlist->steps kwargs)))
      (match rest
        (()
         (values #f inputs #t))
        ((to-realize)
         (receive (to-realize inputs)
             (if (to-realize-arg? to-realize)
                 (values to-realize inputs)
                 (values #f (fold append (alist->steps to-realize) inputs)))
           (values to-realize inputs (not to-realize))))
        ((to-realize rest ...)
         (let*-values (((return-all-realized-results? alists)
                        (if (and (pair? rest) (boolean? (car rest)))
                            (values (car rest) (cdr rest))
                            (values #f rest)))
                       ((to-realize alists)
                        (if (to-realize-arg? to-realize)
                            (values to-realize alists)
                            (values #f (cons to-realize alists)))))
           (values to-realize
                   (append inputs
                           (fold append '() (map alist->steps alists)))
                   (or (not to-realize) return-all-realized-results?))))))))

(define (make-plan-realization realize-step step-result)
  @("Constructs a record to be used for [[#plan-realization|{{plan-realization}}]]."
    (realize-step "A procedure that accepts a {{promise}} which, when
forced, produces a step's result. During realization by
[[#realize-plan|{{realize-plan}}]] it is applied to each step to be
realized exactly once. Its return value is then passed to
{{step-result}}.")
    (step-result "A procedure that accepts one argument which is
whatever is returned by this realization's {{realize-step}}. It is
expected to return the step's result value and may be called multiple
times.")
    (@to "plan-realization")
    (@no-source))
  (void))

(define-record plan-realization
  realize-step step-result)

(define plan-realization-sequential
  @("A sequential plan realization which realizes a plan one step after another."
    (@no-source))
  (make-plan-realization force identity))

(define plan-realization
  @("The current plan realization strategy to be used by
[[#realize-plan|realize-plan]]. The default value is
[[#plan-realization-sequential|{{plan-realization-sequential}}]]. See
[[#make-plan-realization|make-plan-realization]] on how to construct a
custom plan realization."
    (@no-source))
  (make-parameter
   plan-realization-sequential))

(define (realize-plan plan . args)
  @("Realizes the given plan using the current [[#plan-realization|{{plan-realization}}]]."
    (plan "The plan to realize")
    (args "Optional arguments of the following structure:")
    "  (<to-realize> [<return-all-realized-results?>] <input> ... [keyword value] ...)

; to-realize : A step name or a list of step names to be realized. When given, only those steps and their dependencies will be realized.
; return-all-realized-results? : If the second optional argument is {{#t}} then, no matter what is passed as {{to-realize}}, the whole result map will be returned.
; input ... : Zero or more alists with keys being step names and values being constant results. Later alists override previous ones.
; keyword value : Zero or more keyword/value pairs. The keyword will be turned in to symbols and used as step names with their respective values as results.
"
    (@to "result")
    (@no-source))
  (receive (to-realize inputs return-all-realized-results?)
      (destructure-realization-args args)
    (let* ((steps (map-merge (plan-steps plan) (alist->map inputs)))
           (to-realize* (cond ((not to-realize)
                               (map-keys steps))
                              ((list? to-realize)
                               to-realize)
                              (else
                               (list to-realize))))
           (dependencies (step-dependencies to-realize* steps))
           (to-realize* (let loop ((deps (topological-sort dependencies equal?))
                                   (to-realize* '()))
                          (if (null? deps)
                              to-realize*
                              (loop (cdr deps)
                                    (cons (cons (car deps) (map-ref steps (car deps)))
                                          to-realize*)))))
           (step-result  (plan-realization-step-result (plan-realization)))
           (realize-step (plan-realization-realize-step (plan-realization)))
           (step-result*  (lambda (step)
                            (if (car step)
                                (step-result (cdr step))
                                (cdr step))))
           (result-ref (lambda (x results)
                         (step-result* (map-ref results x))))
           (results (realize-steps to-realize* realize-step step-result* result-ref)))
      (cond (return-all-realized-results?
             (map-collect (lambda (name result)
                            (cons name (step-result* result)))
                          results))
            ((list? to-realize)
             (map (lambda (c)
                    (result-ref c results))
                  to-realize))
            (else
             (result-ref to-realize results))))))

(define (merge-plans . plans)
  @("Merges the given {{plans}} from right to left."
    (@to "plan")
    (@no-source))
  (let ((steps (map->transient-map (persistent-map))))
    (for-each
     (lambda (plan)
       (map-each
        (lambda (k v)
          (map-add! steps k v))
        (plan-steps plan)))
     plans)
    (make-plan (persist-map! steps))))

(define-syntax plan-let
  @("A form for declaring a plan which will be realized on evaluation, binding each step to local variables."
    (@to "value of last body expression")
    (@no-source))
  (syntax-rules ()
    ((_ ((step def ...) ...) body ...)
     (match-let (((step ...) (realize-plan (plan (step def ...) ...) '(step ...))))
       body ...))))

(define (write-plan-dot plan #!optional (out (current-output-port)) (name "plan"))
  @("Writes a Graphviz {{DOT}} representation of {{plan}} to the given {{out}} port."
    (@no-source))
  (fprintf out "digraph ~S {~%" (->string name))
  (for-each (lambda (step)
              (let ((name (->string (car step))))
                (for-each (lambda (dep)
                            (fprintf out " ~S -> ~S;~%" dep name)) ;
                          (or (cadr step) '()))))
            (plan-steps plan))
  (fprintf out "}~%"))
