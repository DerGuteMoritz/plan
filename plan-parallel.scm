(module plan-parallel

(realize-plan-parallel
 plan-realization-parallel)

(import chicken scheme)
(use srfi-18 plan)

(define *exn* '(*exn*))

(define (future-force promise)
  (thread-start!
   (lambda ()
     (handle-exceptions exn
       (cons *exn* exn)
       (force promise)))))

(define (future-result thread)
  (let ((result (thread-join! thread)))
    (if (and (pair? result) (eq? *exn* (car result)))
        (abort (cdr result))
        result)))

(define plan-realization-parallel
  (make-plan-realization future-force future-result))

(define (realize-plan-parallel . args)
  (parameterize ((plan-realization plan-realization-parallel))
    (apply realize-plan args)))

)
